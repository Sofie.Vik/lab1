package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    String prompt = "Your choice (Rock/Paper/Scissors)?";
    String prompt2 = "Do you wish to continue playing? (y/n)?";

    boolean continueGame = true;
    String winner;
    boolean newRound = true;

    public void run() {
        while (continueGame == true) {

            if (newRound == true){
               System.out.println("Let's play round " + roundCounter); 
            }
            newRound = true;

            // kode for å skaffe input
            // String computerChoice = computerMove();

            String computerChoice = computerMove();
            String humanChoice = readInput(prompt);
            humanChoice = humanChoice.toLowerCase().strip();
            
            // computer wins

             if ((computerChoice.equals("scissors") && humanChoice.equals("paper")) || (computerChoice.equals("paper") && humanChoice.equals("rock")) || (computerChoice.equals("rock") && humanChoice.equals("scissors"))){
                winner = "Computer wins";
                computerScore++;
            }
            else if ((computerChoice.equals("rock") && humanChoice.equals("paper")) || (computerChoice.equals("paper") && humanChoice.equals("scissors")) || (computerChoice.equals("scissors") && humanChoice.equals("rock"))){
                humanScore++;
                winner = "Human wins";
            }
            else if (computerChoice.equals(humanChoice)) {
                winner = "It's a tie";
            }
            else {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                newRound = false;
                continue;
            }

            System.out.println("Human chose " + humanChoice + ", computer chose " + computerChoice + ". " + winner + "!");
            System.out.println("Score: human " + humanScore + ", computer " + computerScore);
            
            roundCounter++;

            String play = readInput(prompt2);
            
            if (play.equals("y")) {
                continueGame = true;
                continue;
            }
            else {
                continueGame = false;
                System.out.println("Bye bye :)");
                break;
            }
        }  
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */

    public String computerMove(){
        Random random = new Random();
        int randInt = random.nextInt(3);
        String computer = rpsChoices.get(randInt);
        return computer;
    }

    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
